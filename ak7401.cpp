#include "ak7401.h"


/**
 * Constructor.
 *
 */
AK7401::AK7401(){
    _spi = NULL;
    _cs = NULL;
    AK7401::operationMode = AK7401::AK7401_NORMAL_MODE;
}

/**
 * Destructor.
 *
 */
AK7401::~AK7401(){
    if (_spi) delete _spi;
    if(_cs) delete _cs;
}

/**
 * begin
 *
 * @param *spi pointer to SPI instance
 * @param *cs pointer to DigitalOut instance for CS
 */
void AK7401::begin(SPI *spi, DigitalOut *cs){
    if (_spi) delete _spi; 
    _spi=spi;
    if (_cs) delete _cs;
    _cs=cs;
    _cs->write(1);
}

/**
 * Soft reset. 
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7401::Status AK7401::softReset() {
    char data[2] = {0x00,0x00};    
    AK7401::Status status = AK7401::write(AK7401_OPCODE_RESET, 0x4E, data);
    AK7401::operationMode = AK7401::AK7401_NORMAL_MODE;
    return status;    
}

/**
 * Check the connection. 
 *
 * @note Connection check is performed by reading a register which has a fixed value and verify it.
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7401::Status AK7401::checkConnection() {
    AK7401::Status status;

    // store the original mode
    AK7401::OperationMode org = AK7401::operationMode;
    
    // set to user mode
    AK7401::setOperationMode(AK7401::AK7401_USER_MODE);

    // read current mode, should be AK7401_USER_MODE:0x0F05
    char data[2] = {0x00,0x00};
    AK7401::readRegister(AK7401_REG_CHMD, data);  
    if( (data[0]&0x0F) == 0x05 && data[1] == 0x0F) status = AK7401::SUCCESS;
    else status = AK7401::ERROR;

    // back to the original mode
    AK7401::setOperationMode(org);

    return status;

}

/** 
 * Writes data to EEPROM on the device. 
 * @param address EEPROM address
 * @param data data to be written
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7401::Status AK7401::writeEEPROM(char address, const char *data){
    if(AK7401::operationMode != AK7401::AK7401_USER_MODE) return AK7401::ERROR_IN_NORMAL_MODE;
    AK7401::Status status = AK7401::write(AK7401_OPCODE_WRITE_EEPROM, address, data);
    wait(0.02);    // Wait Wt=20msec(MAX)
    return status;
}
/** 
 *  Reads data from EEPROM on the device. 
 * @param address EEPROM address
 * @param data data to read
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7401::Status AK7401::readEEPROM(char address, char *data){
    if(AK7401::operationMode != AK7401::AK7401_USER_MODE) return AK7401::ERROR_IN_NORMAL_MODE;
    AK7401::Status status = read(AK7401_OPCODE_READ_EEPROM, address, data);
    return status;
}

/** 
 * Writes data to register on the device. 
 * @param address register address
 * @param data data to be written
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7401::Status AK7401::writeRegister(char address, const char *data){
    if(AK7401::operationMode != AK7401::AK7401_USER_MODE) return AK7401::ERROR_IN_NORMAL_MODE;
    AK7401::Status status = AK7401::write(AK7401_OPCODE_WRITE_REGISTER, address, data);
    wait_us(5);
    return status;
}

/** 
 *  Reads data from register on the device. 
 * @param address register address
 * @param data data to read
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7401::Status AK7401::readRegister(char address, char *data){
    if(AK7401::operationMode != AK7401::AK7401_USER_MODE) return AK7401::ERROR_IN_NORMAL_MODE;
    AK7401::Status status = AK7401::read(AK7401_OPCODE_READ_REGISTER, address, data);
    return status;
}

/**
 * Sets device operation mode.
 *
 * @param mode device opration mode
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7401::Status AK7401::setOperationMode(AK7401::OperationMode mode){

    char command[2];
    command[0] = (char)(0x0F&(mode>>8));
    command[1] = (char)(0x00FF&mode);

    AK7401::operationMode = mode;
    AK7401::Status status = AK7401::write(AK7401_OPCODE_CHANGE_MODE, AK7401_REG_CHMD, command);
    wait(0.01); // this wait for 
    return status;
}

/**
 * Gets device operation mode.
 *
 * @return Returns OperationMode.
 */
AK7401::OperationMode AK7401::getOperationMode(){
    return AK7401::operationMode;
}

/**
 * Reads angle data from the device.
 *
 * @param angle pointer to read angle data buffer
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7401::Status AK7401::readAngle(char *angle) {

    char data[2]={0x00,0x00};

    if( AK7401::read(AK7401_OPCODE_READ_ANGLE, AK7401_REG_ANG, data) != AK7401::SUCCESS ) return AK7401::ERROR;

    // status check
    if( (data[0] & AK7401_READ_ANGLE_STATE_ER) == 0 ){
        return AK7401::ERROR_ABNORMAL_STRENGTH;
    }
    // parity check
    if( AK7401::parityCheck(data) != AK7401::SUCCESS ) return AK7401::ERROR_PARITY;
    
    angle[0] = data[0]&0x0F;
    angle[1] = data[1];

    return AK7401::SUCCESS;
}

/**
 * Measures and reads angle, magnetic flux density and abnormal state code while in the user mode.
 *
 * @param angle pointer to angle data 
 * @param density magnetic flux density
 * @param abnormal_state abnormal state
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7401::Status AK7401::readAngleMeasureCommand(char *angle, char *density, char *abnormal_state) {
    AK7401::OperationMode org_mode = AK7401::operationMode;
    
    if(org_mode == AK7401::AK7401_USER_MODE){
        AK7401::setOperationMode(AK7401::AK7401_NORMAL_MODE);
    }
    
    AK7401::Status status;
    char data[2] = {0x00,0x00};
    
    status = AK7401::read(AK7401_OPCODE_READ_ANGLE, AK7401_REG_ANG, data);
    *abnormal_state = (data[0]>>4)&0x07;
    angle[0] = data[0]&0x0F;
    angle[1] = data[1];

    AK7401::setOperationMode(org_mode);
    
    // mag density read
    data[0] = 0x00;
    data[1] = 0x00;
    status = AK7401::readRegister(AK7401_REG_MFDI, data);
    if(status != AK7401::SUCCESS) return status;
    *density = data[1];
    
    return AK7401::SUCCESS;
}


/**
 * Measures current angle and sets the value to EEPROM as zero angle position.
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
 AK7401::Status AK7401::setAngleZero(){

    AK7401::Status status;
    
    // store the original mode
    AK7401::OperationMode org = AK7401::operationMode;
    
    // set to user mode
    status = AK7401::setOperationMode(AK7401::AK7401_USER_MODE);
    if(status != AK7401::SUCCESS) return status;        
    
    // set the R_ZP register to 0x00, 0x00
    char angle[2] = {0x00,0x00};
    status = AK7401::writeEEPROM(AK7401_REG_ZP, angle);
    if(status != AK7401::SUCCESS) return status;
    
    // set to normal mode to get angle data
    status = AK7401::setOperationMode(AK7401::AK7401_NORMAL_MODE);
    if(status != AK7401::SUCCESS) return status;
    
    // measure angle data without zero offset
    status = AK7401::readAngle(angle);
    if(status != AK7401::SUCCESS) return status;
    
    // back again to the user mode to set R_ZP
    status = AK7401::setOperationMode(AK7401::AK7401_USER_MODE);
    if(status != AK7401::SUCCESS) return status;        
    
    // set R_ZP register with read angle data
    status = AK7401::writeRegister(AK7401_REG_ZP, angle);
    if(status != AK7401::SUCCESS) return status;
    
    // set R_ZP EEPROM with read angle data
    status = AK7401::writeEEPROM(AK7401_REG_ZP, angle);
    if(status != AK7401::SUCCESS) return status;
    
    // back to the original mode
    status = AK7401::setOperationMode(org);
    if(status != AK7401::SUCCESS) return status;        
    
    return status;
}

/**
 * Sets the value to EEPROM as zero angle position.
 *
 * @param angle zero angle position
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
//AK7401::Status AK7401::setAngleZero(const char *angle){
//    if(AK7401::operationMode != AK7401::AK7401_USER_MODE) return AK7401::ERROR_IN_NORMAL_MODE;
//    return AK7401::writeEEPROM(AK7401_REG_ZP, angle);
//}

////////////////////////////////////////////////////////
// private methods
////////////////////////////////////////////////////////


/** 
 *  Reads data from device. 
 * @param operation_code OPCODE 
 * @param address memory/register addredd
 * @param *data pointer to the read buffer. length=2 fixed.
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7401::Status AK7401::read(char operation_code, char address, char *data) {

	char command[3];

	command[0] = (operation_code<<4) | (address>>3);
    command[1] = 0xF0&(address<<5);
    command[2] = 0x00;
    
    _cs->write(0);

    _spi->write(command[0]);
    for(int i=0; i<2; i++){
        data[i] = _spi->write(command[i+1]);
    }

    wait_us(1);

    _cs->write(1);
    
    return AK7401::SUCCESS;
}

/** 
 * Writes data to the device. 
 * @param operation_code OPCODE 
 * @param address memory/register addredd
 * @param *data pointer to the read buffer. length=2 fixed.
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7401::Status AK7401::write(char operation_code, char address, const char *data) {

	char command[3];

	command[0] = (operation_code<<4) | (address>>3);
    command[1] = (0xF0&(address<<5)) | (0x0F&data[0]);
    command[2] = data[1];
    
    _cs->write(0);

    for(int i=0; i<3; i++){
        _spi->write(command[i]);
    }

    wait_us(1);

    _cs->write(1);
    
    return AK7401::SUCCESS;
}

/** 
 * Checks parity bits sub function
 * @param data data 12bit read data
 * @param parity parity bit status
 * @param error error bit status
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7401::Status AK7401::parityCheckSub(const char data, const char parity, const char error){

	char sum = parity + error;

    for(int i=0; i<6; i++){
        if( ((data>>i) & 0x01) !=0 )sum++;
    }

    if( (sum & 0x01) == 0)
    	return AK7401::SUCCESS;
    else
    	return AK7401::ERROR_PARITY;
}

/** 
 * Checks parity bits
 * @param data 2 byte read data to chek parity
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7401::Status AK7401::parityCheck(const char *data){

	AK7401::Status status;
    char p1 = 0x01 & (data[0]>>6);
    char p2 = 0x01 & (data[0]>>5);
    char er = 0x01 & (data[0]>>4);
    char hi = ( (data[0]&0x0F)<<2 ) | ((data[1] & 0xC0)>>6);    // Data[11:6]
    char lo = data[1] & 0x3F;                                   // Data[5:0]

    status = AK7401::parityCheckSub(hi, p1, er);

    if( status != AK7401::SUCCESS) return AK7401::ERROR_PARITY;

    status = parityCheckSub(lo, p2, er);

    if( status != AK7401::SUCCESS) return AK7401::ERROR_PARITY;

    return SUCCESS;
}
