#ifndef AK7401_H
#define AK7401_H

#include "mbed.h"

// OPCODE Specification
#define AK7401_OPCODE_WRITE_EEPROM          0x01    // Able to write a data on EEPROM
#define AK7401_OPCODE_READ_EEPROM           0x02    // Able to read an EEPROM
#define AK7401_OPCODE_WRITE_REGISTER        0x03    // Able to write a data on register
#define AK7401_OPCODE_READ_REGISTER         0x04    // Able to read a register
#define AK7401_OPCODE_CHANGE_MODE           0x05    // Able to change mode
#define AK7401_OPCODE_RESET                 0x06    // Able to reset the AK7401
#define AK7401_OPCODE_TRIGGER               0x07    // Updates the processing data
#define AK7401_OPCODE_READ_ANGLE            0x09    // Able to read an angle data

// Register Address Map
#define AK7401_REG_ANG                      0x00    // 12bit angle data
#define AK7401_REG_MFDI                     0x01    // Magnetic flux density strength(1LSB/mT)
#define AK7401_REG_CHMD                     0x02    // For mode state
#define AK7401_REG_MLK                      0x03    // For memory lock
#define AK7401_REG_ID                       0x04    // For ID data (EEPROM Only)
#define AK7401_REG_RD                       0x05    // Rotation Direction Configuration
#define AK7401_REG_ZP                       0x06    // Zero Degree Point Configuration
#define AK7401_REG_ABNRM                    0x07    // Self-diagnistic configuration
#define AK7401_REG_MFDRH                    0x08    // Upper Linit of Magnetic Flux Density
#define AK7401_REG_MFDRL                    0x09    // Lower Linit of Magnetic Flux Density
#define AK7401_REG_PWMPL                    0x0A    // PWM Configuration
#define AK7401_REG_PWMOMD                   0x0B    // Output type of the OUT Pin Congiguration
#define AK7401_REG_IT                       0x0D    // Analog Data Averaging Configration

#define AK7401_BIT_MASK_CCW                 0x00    // Counter clockwise rotation
#define AK7401_BIT_MASK_CW                  0x07    // Clockwise rotation

#define AK7401_BIT_MASK_ABNRM_OVER_VOLTAGE    0x0800
#define AK7401_BIT_MASK_ABNRM_UNDER_VOLTAGE   0x0400
#define AK7401_BIT_MASK_ABNRM_EEPROM_WRITE    0x0200
#define AK7401_BIT_MASK_ABNRM_AVERAGE         0x0100
#define AK7401_BIT_MASK_ABNRM_EEPROM_READ     0x0080
#define AK7401_BIT_MASK_ABNRM_PWM_DUTY        0x0040
#define AK7401_BIT_MASK_ABNRM_UNDER_MAGF      0x0020
#define AK7401_BIT_MASK_ABNRM_ANALOG          0x0010
#define AK7401_BIT_MASK_ABNRM_CONF_VS_TRANS   0x0008
#define AK7401_BIT_MASK_ABNRM_TRANS_VS_EEPROM 0x0004
#define AK7401_BIT_MASK_ABNRM_TRIPLE_EEPROM   0x0002
#define AK7401_BIT_MASK_ABNRM_IC_STARTUP      0x0001

#define AK7401_READ_ANGLE_STATE_P1          0x40
#define AK7401_READ_ANGLE_STATE_P2          0x20
#define AK7401_READ_ANGLE_STATE_ER          0x10

#define AK7401_ABNORMAL_STATE_NORMAL        0x03    //

#define AK7401_LEN_BUF_MAX					0x03	// 2-byte length, plus 1 byte for address

/**
 * This is a device driver of AK7401 with SPI interface.
 *
 * @note AK7401 is a high speed angle sensor IC manufactured by AKM.
 * 
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak7401.h"
 * 
 * 
 * int main() {
 *     // Creates an instance of SPI
 * }
 * @endcode
 */
class AK7401
{
public:

    /**
     * Available opration modes in AK7401.
     */
    typedef enum {
        AK7401_NORMAL_MODE  = 0x0000,   /**< Normal mode operation. */
        AK7401_USER_MODE    = 0x050F,   /**< User mode operation. */
    } OperationMode;

    /**
     * Status of function. 
     */
    typedef enum {
        SUCCESS,                    /**< The function processed successfully. */
        ERROR,                      /**< General Error */
        ERROR_IN_USER_MODE,         /**< Error in user mode. */
        ERROR_IN_NORMAL_MODE,       /**< Error in normal mode. */
        ERROR_PARITY,               /**< Parity bit error. */
        ERROR_ABNORMAL_STRENGTH,    /**< Abnormal strength error. */
    } Status;

    /**
     * Constructor.
     *
     */
    AK7401();

    /**
     * Destructor.
     *
     */
     ~AK7401();

    /**
     * begin
     *
     * @param *spi pointer to SPI instance
     * @param *cs pointer to DigitalOut instance for CS
     */
    void begin(SPI *spi, DigitalOut *cs);
    
    /**
     * Check the connection. 
     *
     * @note Connection check is performed by reading a register which has a fixed value and verify it.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status checkConnection();

    /**
     * Soft reset. 
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status softReset();

    /** 
     * Writes data to EEPROM on the device. 
     * @param address EEPROM address
     * @param data data to be written
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status writeEEPROM(char address, const char *data);

    /** 
     *  Reads data from EEPROM on the device. 
     * @param address EEPROM address
     * @param data data to read
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status readEEPROM(char address, char *data);

    /** 
     * Writes data to register on the device. 
     * @param address register address
     * @param data data to be written
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status writeRegister(char address, const char *data);

    /** 
     *  Reads data from register on the device. 
     * @param address register address
     * @param data data to read
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status readRegister(char address, char *data);

    /**
     * Sets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status setOperationMode(OperationMode mode);

    /**
     * Gets device operation mode.
     *
     * @return Returns OperationMode.
     */
    OperationMode getOperationMode();


    /**
     * Reads angle data from the device.
     *
     * @param angle pointer to read angle data buffer
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status readAngle(char *angle);
    
    /**
     * Measures and reads angle, magnetic flux density and abnormal state code while in the user mode.
     *
     * @param angle pointer to angle data buffer
     * @param density magnetic flux density
     * @param abnormal_state abnormal state
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status readAngleMeasureCommand(char *angle, char *density, char *abnormal_state);
    
    /**
     * Measures current angle and sets the value to EEPROM as zero angle position.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    Status setAngleZero();
    
    /**
     * Sets the value to EEPROM as zero angle position.
     *
     * @param angle zero angle position
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
//    Status setAngleZero(const char *angle);


private:        
    /**
     * Holds a pointer to an SPI object. 
     */
    SPI *_spi;

    /**
     * Holds a DigitalOut oblject for CS; 
     */
    DigitalOut *_cs;
    
    /**
     * Holds current mode 
     */
    OperationMode operationMode;

    /** 
     *  Reads data from device. 
     * @param operation_code OPCODE 
     * @param address memory/register addredd
     * @param *data pointer to the read buffer. length=2 fixed.
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status read(char operation_code, char address, char *data);

    /** 
     * Writes data to the device. 
     * @param operation_code OPCODE 
     * @param address memory/register addredd
     * @param *data pointer to the read buffer. length=2 fixed.
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status write(char operation_code, char address, const char *data);

    /** 
     * Checks parity bits sub function
     * @param data data 12bit read data
     * @param parity parity bit status
     * @param error error bit status
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status parityCheckSub(const char data, const char parity, const char error);

    /** 
     * Checks parity bits
     * @param data 2 byte read data to chek parity
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    Status parityCheck(const char *data);
};

#endif
